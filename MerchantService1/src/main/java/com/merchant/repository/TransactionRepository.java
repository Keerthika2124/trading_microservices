package com.merchant.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.merchant.entity.Transactions;

public interface TransactionRepository extends JpaRepository<Transactions, Integer> {

}
