package com.merchant.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.merchant.entity.Merchants;

public interface MerchantRepository extends JpaRepository<Merchants, Integer>{

}
