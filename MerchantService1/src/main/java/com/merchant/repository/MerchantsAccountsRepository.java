package com.merchant.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.merchant.entity.MerchantAccounts;
import com.merchant.entity.Merchants;

public interface MerchantsAccountsRepository extends JpaRepository<MerchantAccounts, Integer> {

	Optional<MerchantAccounts> findByMerchant(Merchants merchant);

}
