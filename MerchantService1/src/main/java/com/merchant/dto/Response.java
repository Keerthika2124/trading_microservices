package com.merchant.dto;

import lombok.Builder;

@Builder
public record Response(ApiResponse apiResponse, OrderDto orderDto ) {

}
