package com.merchant.dto;

import java.math.BigDecimal;

import lombok.Builder;

@Builder

public record OrderDto(Integer userId, Integer stockId, Integer orderQuantity, BigDecimal totalPrice) {

}
