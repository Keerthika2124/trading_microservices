package com.merchant.service;

import java.math.BigDecimal;

import com.merchant.dto.ApiResponse;
import com.merchant.dto.Response;

public interface MerchantService {

	ApiResponse creditMerchant(Integer merchantId, BigDecimal totalPrice);

	Response fetchOrders();

}
