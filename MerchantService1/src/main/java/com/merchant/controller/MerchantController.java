package com.merchant.controller;

import java.math.BigDecimal;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.merchant.dto.ApiResponse;
import com.merchant.dto.Response;
import com.merchant.service.MerchantService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/merchants")
@RequiredArgsConstructor
public class MerchantController {
	
	private final MerchantService merchantService;
	
	@PostMapping("/{merchantId}")
	public ResponseEntity<ApiResponse> creditMerchant(@PathVariable Integer merchantId, @RequestParam BigDecimal totalPrice){
		return new ResponseEntity<>(merchantService.creditMerchant(merchantId, totalPrice),HttpStatus.OK);
	} 
	
	@GetMapping("")
	public ResponseEntity<Response> fetchOrders(){
		return new ResponseEntity<>(merchantService.fetchOrders(),HttpStatus.OK);
	}
}
