package com.merchant.serviceimpl;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.merchant.dto.ApiResponse;
import com.merchant.dto.Response;
import com.merchant.entity.MerchantAccounts;
import com.merchant.entity.Merchants;
import com.merchant.entity.Status;
import com.merchant.entity.Transactions;
import com.merchant.exceptions.MerchantNotFoundException;
import com.merchant.repository.MerchantRepository;
import com.merchant.repository.MerchantsAccountsRepository;
import com.merchant.repository.TransactionRepository;
import com.merchant.service.MerchantService;
import com.merchant.utils.Responses;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class MerchantServiceImpl implements MerchantService {

	private final MerchantRepository merchantRepository;
	private final MerchantsAccountsRepository merchantsAccountsRepository;
	private final TransactionRepository transactionRepository;
	

	@Override
	@Transactional
	public ApiResponse creditMerchant(Integer merchantId, BigDecimal totalPrice) {
		System.out.println("Inside credit");
		Merchants merchant = merchantRepository.findById(merchantId).orElseThrow(() -> {
			log.error(Responses.MERCHANT_NOT_FOUND_MESSAGE);
			throw new MerchantNotFoundException(Responses.MERCHANT_NOT_FOUND_MESSAGE,
					Responses.MERCHANT_NOT_FOUND_CODE);
		});
		MerchantAccounts accounts = merchantsAccountsRepository.findByMerchant(merchant).orElseThrow(() -> {
			log.error(Responses.MERCHANTACCOUNT_NOT_FOUND_MESSAGE);
			throw new MerchantNotFoundException(Responses.MERCHANTACCOUNT_NOT_FOUND_MESSAGE,
					Responses.MERCHANTACCOUNT_NOT_FOUND_CODE);
		});
		
		BigDecimal accBal =(accounts.getBalance()).add(totalPrice);
		accounts.setBalance(accBal);
		merchantsAccountsRepository.save(accounts);
		
		Transactions transactions=new Transactions();
		transactions.setAmount(totalPrice);
		transactions.setMerchant(merchant);
		transactions.setStatus(Status.SUCCESSFUL);
		transactionRepository.save(transactions);
		
		log.info("Adding balance to merchant Id", +merchantId);
		return new ApiResponse(Responses.DISPLAY_DATA_CODE, Responses.DISPLAY_DATA_MESSAGE);
	}

	@Override
	public Response fetchOrders() {
		
		return null;
	}
}
