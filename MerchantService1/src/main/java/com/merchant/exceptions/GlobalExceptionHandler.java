package com.merchant.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.merchant.utils.Responses;

public class GlobalExceptionHandler {

	@ExceptionHandler(MerchantNotFoundException.class)
	public ResponseEntity<ErrorResponse> adminExceptionHandler(MerchantNotFoundException ex) {
		ErrorResponse error = new ErrorResponse(Responses.MERCHANT_NOT_FOUND_CODE, ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

}
