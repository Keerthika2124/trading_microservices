package com.merchant.exceptions;

public class MerchantNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public MerchantNotFoundException(String code, String message) {
		super(message);
		this.code = code;
	}

}
