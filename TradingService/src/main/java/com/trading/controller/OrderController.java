package com.trading.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.trading.dto.ApiResponse;
import com.trading.service.OrderService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/orders")
public class OrderController {

	private final OrderService orderService;

	public OrderController(OrderService orderService) {
		super();
		this.orderService = orderService;
	}

	@PostMapping()
	public ResponseEntity<ApiResponse> placeOrder(@RequestParam Integer userId, @RequestParam Integer stockId,
			@RequestParam Integer orderQuantity, @RequestParam Integer merchantId) {
		return new ResponseEntity<>(orderService.orderStock(userId, stockId, orderQuantity, merchantId),
				HttpStatus.OK);
	}
}
