package com.trading.exceptions;


public class UserNotFoundException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public UserNotFoundException(String message) {
		super(message,"400001");
	}
	public UserNotFoundException(String message,String code) {
		super(message,code);
	}


}
