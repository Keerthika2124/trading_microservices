package com.trading.external;

import java.math.BigDecimal;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.trading.dto.ApiResponse;


@FeignClient(value = "MerchantService1", url = "http://localhost:8082/merchant")
public interface MerchantServiceClient {

//	@GetMapping("/{merchantId}")
//	public ResponseEntity<ApiResponse> creditMerchant(@PathVariable Integer merchantId);

	@PostMapping("/api/v1/merchants/{merchantId}")
	public ResponseEntity<ApiResponse> creditMerchant(@PathVariable("merchantId") Integer merchantId, @RequestParam BigDecimal totalPrice);
}