package com.trading.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trading.entity.Stocks;

public interface StocksRepository extends JpaRepository<Stocks, Integer> {

}
