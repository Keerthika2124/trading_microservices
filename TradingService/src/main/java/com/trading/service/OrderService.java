package com.trading.service;

import com.trading.dto.ApiResponse;

public interface OrderService {

	ApiResponse orderStock(Integer userId, Integer stockId, Integer orderQuantity, Integer merchantId);
}
