package com.trading.dto;

import lombok.Builder;

@Builder
public record OrderDto(Integer userId, Integer stockId, Integer orderQuantity) {

}
