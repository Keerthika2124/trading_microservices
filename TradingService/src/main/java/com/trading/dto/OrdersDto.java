package com.trading.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrdersDto {

	private Integer userId;
	private Integer stockId;
	private Integer orderQuantity;
	private String stockName;
	private BigDecimal stockPrice;
	private LocalDate orderDate;
	private String tradeType;
	private String orderStatus;

	
}
