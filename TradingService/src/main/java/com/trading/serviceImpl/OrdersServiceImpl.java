package com.trading.serviceImpl;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.trading.dto.ApiResponse;
import com.trading.dto.OrdersDto;
import com.trading.entity.Accounts;
import com.trading.entity.OrderStatus;
import com.trading.entity.Orders;
import com.trading.entity.Stocks;
import com.trading.entity.UserDetails;
import com.trading.exceptions.GlobalErrorCode;
import com.trading.exceptions.InsufficientBalanceException;
import com.trading.exceptions.StockNotFoundException;
import com.trading.exceptions.UserNotFoundException;
import com.trading.external.MerchantServiceClient;
import com.trading.repository.AccountsRepository;
import com.trading.repository.OrdersRepository;
import com.trading.repository.StocksRepository;
import com.trading.repository.UserDetailsRepository;
import com.trading.service.OrderService;
import com.trading.utils.Responses;

import jakarta.transaction.Transactional;

@Service
public class OrdersServiceImpl implements OrderService {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final OrdersRepository ordersRepository;
	private final AccountsRepository accountRepository;
	private final StocksRepository stocksRepository;
	private final UserDetailsRepository userRepository;
	public final MerchantServiceClient merchantServiceClient;

	public OrdersServiceImpl(OrdersRepository ordersRepository, AccountsRepository accountRepository,
			StocksRepository stocksRepository, UserDetailsRepository userRepository,
			MerchantServiceClient merchantServiceClient) {
		super();
		this.ordersRepository = ordersRepository;
		this.accountRepository = accountRepository;
		this.stocksRepository = stocksRepository;
		this.userRepository = userRepository;
		this.merchantServiceClient = merchantServiceClient;
	}

	@Transactional
	public ApiResponse orderStock(Integer userId, Integer stockId, Integer orderQuantity, Integer merchantId) {

		Orders orders = new Orders();
		logger.info("ordering stock - orderStockService");
		UserDetails user = userRepository.findById(userId).orElseThrow(() -> {
			logger.error(Responses.USER_NOT_FOUND_MESSAGE);
			throw new UserNotFoundException(Responses.USER_NOT_FOUND_MESSAGE);
		});

		Accounts account = accountRepository.findByUser(user).orElseThrow(() -> {
			logger.error(Responses.ACCOUNT_NOT_FOUND_MESSAGE);
			throw new UserNotFoundException(Responses.ACCOUNT_NOT_FOUND_MESSAGE);
		});

		Stocks stock = stocksRepository.findById(stockId).orElseThrow(() -> {
			logger.error(Responses.STOCK_NOT_FOUND_MESSAGE);
			throw new StockNotFoundException(Responses.STOCK_NOT_FOUND_MESSAGE);
		});
		//Integer stockQuantity=stock.setQuantity(stock.getQuantity() - orderQuantity);

		BigDecimal totalPrice = BigDecimal.valueOf(orderQuantity).multiply(stock.getStockPrice());

		if (totalPrice.compareTo(account.getBalance()) > 0) {
			logger.error(Responses.INSUFFICIENT_BALANCE_TO_ORDER_MESSAGE);
			throw new InsufficientBalanceException(Responses.INSUFFICIENT_BALANCE_TO_ORDER_MESSAGE);
		}
		BigDecimal balance = account.getBalance().subtract(totalPrice);
		account.setBalance(balance);
		accountRepository.save(account);

		orders.setUser(user);
		orders.setStock(stock);
		orders.setStockName(stock.getStockName());
		orders.setStockPrice(totalPrice);
		orders.setTradeType("BUY");
		orders.setOrderDate(LocalDate.now());
		orders.setOrderStatus(OrderStatus.PENDING);
		ordersRepository.save(orders);
		merchantServiceClient.creditMerchant(merchantId, totalPrice);
		//stocksRepository.save(stock);
		return new ApiResponse(Responses.ORDERING_STOCK_MESSAGE, Responses.ORDERING_STOCK_CODE);

	}
}
