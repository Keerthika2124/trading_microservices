package com.trading.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Orders {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer orderId;
	@ManyToOne
	private UserDetails user;
	@ManyToOne
	private Stocks stock;
	private Integer orderQuantity;
	private String stockName;
	private BigDecimal stockPrice;
	private LocalDate orderDate;
	private String tradeType;
	@Enumerated(EnumType.STRING)
	private OrderStatus orderStatus;
}
