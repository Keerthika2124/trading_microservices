package com.trading.entity;

public enum OrderStatus {
	PENDING, APPROVED, REJECTED
}
